package fr.rathesh.springbook.springbooktest;

import javax.persistence.*;

@Entity
public class Pokemon {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id = 1;
    private String nom;
    @ManyToOne(cascade = CascadeType.ALL)
    private Espece espece;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setEspece(Espece espece) {
        this.espece = espece;
    }

    public Espece getEspece() {
        return espece;
    }
}
