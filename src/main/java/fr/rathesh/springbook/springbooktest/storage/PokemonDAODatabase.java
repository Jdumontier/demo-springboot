package fr.rathesh.springbook.springbooktest.storage;

import fr.rathesh.springbook.springbooktest.Pokemon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
@ConditionalOnProperty(name = "storagemode", havingValue = "database")
public class PokemonDAODatabase implements PokemonDAO {

    @Value("${connectionURL}")
    private String connectionURL;

    @Autowired
    private PokemonRepository pokemonRepository;

    public Iterable<Pokemon> getMyPokemons() {
        return pokemonRepository.findAll();
        /*
        System.out.println("Using database "+connectionURL);
        List<Pokemon> pokemons = new ArrayList<>();
        Connection con = null;
        try{
            con = DriverManager.getConnection(connectionURL);
            Statement stmt=con.createStatement();
            ResultSet rs=stmt.executeQuery("select * from pokemon");
            while(rs.next()) {
                Pokemon pokemon = new Pokemon();
                pokemon.setId(rs.getInt(1));
                pokemons.add(pokemon);
            }
        }catch(Exception e){ System.out.println(e);}
        finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }
        return pokemons;
        */
    }

    @Override
    public void insererPokemon(Pokemon pokemon) {
        pokemonRepository.save(pokemon);
    }

    public String getConnectionURL() {
        return connectionURL;
    }

    public void setConnectionURL(String connectionURL) {
        this.connectionURL = connectionURL;
    }
}
